<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Arafat Thabet Prtfolio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <link href="fonts/mdi/css/materialdesignicons.min.css" media="all" />
    <link rel="stylesheet" media="all" href="{{ asset ('css/bootstrap.min.css')}}" />

    <link rel="stylesheet" media="all" href="{{ asset ('css/fontawesome.min.css')}}" />
    <link rel="stylesheet" media="all" href="{{ asset ('css/animate.css')}}" />
    <link rel="stylesheet" href="{{ asset ('css/main.css')}}" />

</head>

<body>
<!-- Loading end -->
<style type="text/css">
    /************************************************
  ************************************************
                  14. Loading
  ************************************************
************************************************/

    #loading-wrapper {
        position: fixed;
        top: 0;
        left: 0;
       /* display: none;*/
        width: 100%;
        height: 100%;
        min-height: 800px;
        z-index: 5000;
         background: #e6ecf3;
    }

    #loading-wrapper #loader {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -30px;
        margin-top: -30px;
        width: 60px;
        height: 60px;
    }

    #loading-wrapper #loader:before {
        content: '';
        box-sizing: border-box;
        position: absolute;
        top: 50%;
        left: 50%;
        width: 48px;
        height: 48px;
        margin-left: -24px;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
        border: 2px solid #d0d6e9;
        border-top-color: #118cf1;
        -moz-animation: spinner .6s linear infinite;
        -webkit-animation: spinner .6s linear infinite;
        animation: spinner .6s linear infinite;
    }

    @-moz-keyframes spinner {
        from {
            transform: rotate(0deg);
        }

        to {
            transform: rotate(360deg);
        }
    }

    @-webkit-keyframes spinner {
        from {
            transform: rotate(0deg);
        }

        to {
            transform: rotate(360deg);
        }
    }

    @keyframes spinner {
        from {
            transform: rotate(0deg);
        }

        to {
            transform: rotate(360deg);
        }
    }
   /* 14. Loading
  ************************************************
************************************************/
    </style>
                 
		<!-- Loading start -->
		<div id="loading-wrapper">
			<div id="loader"></div>
		</div>
		<!-- Loading end -->
    <!-- HOME -->
  
    <section id="home" class=" home">
     
        <div id="pt" class="canvas"></div>
        <div class="home-txt animated zoomIn wow">
            <p>Hello, I'm <span>Arafat Thabet</span>. <br>I'm a full-stack web developer.</p>
            <a href="#projects" class="btn btn-contact ">Show my Work</a>
        </div>
  
    </section>


    <!-- Navbar -->
    <nav class=" navbar-expand-md navbar-dark desk main-nav">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarContent">
          <ul class="navbar-nav mr-auto pl-2">
          @foreach($navbar as $nav)
            <li class="page-link">
                <a class="nav-link" href="{{$nav->url}}">{{$nav->name}}</a>
            </li>
            @endforeach
           
          </ul>
        </div>
      </nav>

 <!-- services -->
 <section id="services"  class="services py-5">
        <div class="row">
            <div class="container">
                <div class="col-md-12">
                    <div class="section-header ">
                        <div class=" text-center ">
                            <h2 class="animated fadeInDown wow" data-wow-delay=".5s"><span>Services</span></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4  col-sm-6 mt-3">
                        <div class="srevice animated fadeInUp wow">
                            <i class="fa fa-4x fa-tachometer"></i>
                            <h3>Fast</h3>
                            <p>Fast load times and lag free interaction, my highest priority.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 mt-3">
                        <div class="srevice  animated fadeInUp wow">
                            <i class="fa fa-4x fa-tablet"></i>
                            <h3>100% Responsive</h3>
                            <p> My layouts will work on any device,     big or small.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 mt-3 animated fadeInUp wow">
                        <div class="srevice">
                            <i class="fa fa-4x fa-eye-slash"></i>
                            <h3>Awesome Display</h3>
                            <p>I Create Your Digital Dreams Just Think And Tell Me What You Need.
                            </p>
                        </div>
                    </div>
                    <br>
                    <div class="col-md-4 col-sm-6 mt-3 animated fadeInUp wow">
                        <div class="srevice">
                            <i class="fa fa-4x fa-sliders"></i>
                            <h3>API Development</h3>
                            <p>build  Web Services , deal with global web services .
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6  mt-3 animated fadeInUp wow">
                        <div class="srevice">
                            <i class="fa fa-4x fa-wrench"></i>
                            <h3>Maintenance</h3>
                            <p>Maintenance websites, web systems ,database Structure.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6  mt-3 animated fadeInUp wow">
                        <div class="srevice">
                            <i class="fa fa-4x fa-rocket"></i>
                            <h3>Dynamic</h3>
                            <p>Websites don't have to be static, I love making pages come to life.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
   

</Section>
 <!-- End services -->
    <!-- ABOUT -->
    <section  class="about pt-5">
        <div class="row">
            <div class="container">
                <div class="col-md-12">
                    <div class="section-header ">
                        <div class=" text-center ">
                            <h2 class="animated fadeInDown wow" data-wow-delay=".5s"><span>About</span></h2>
                        </div>
                    </div>
                </div>
            </div>
           
            
        </div>
        <div id="about" class="my-info">
            <div class="row">
                <div class="col-xs-12 col-md-4 ">
                <?php
                                if ($about->img)
                                    $about_img = asset('storage/' . $about->img );
                                else
                                    $about_img = asset('img/avatar.jpg');
                                ?>
                                <div>
                    <img class="animated img-thumbnail fadeInLeft responsive wow" data-wow-delay=".5s" src="{{$about_img}}">
</div>
                    <div class="morinfo">
               
                    <div  class="animated fadeInUp wow">
                        <h5 class="text-center font-weight-bold">{{$about->title}}</h5>
                        <p class="font-weight-bold"> {{$about->description}}</p>
                        @if(!empty($cv_link))
                            <a class="cv-btn" href="{{$cv_link}}" download=""><i class="fa fa-download"></i>Download CV</a>
                             @endif
                        </div>
                </div>
                </div>
                <div class=" col-xs-12 col-md-6 offset-md-1">
                    <div id="skills" class="skills-bars">
                    @foreach($skills as $skill)
                        <div class="animated skillbar wow fadeInRight ">
                            <div class="skillbar-title">{{$skill->name}}</div>
                            <div class="progress skill-bar-percent">
                                <div class="progress-bar progress-bar-animated" style="width:{{ $skill->percentage }}%"></div>
                                <span>{{ $skill->percentage }}%</span>
                            </div>
                        </div>
                        @endforeach
                    
             
                    </div>
                </div>
            </div>
          
        </div>

</section>
 <!-- End ABOUT -->
    <!-- PORTFOLIO -->
    <section id="projects" class="latest-projects">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-header text-center">
                        <h2 class="animated fadeInUp wow" data-wow-delay=".5s"><span> Latest Projects</span></h2>
                        <p class="line1"></p>
                        <p class="line2"></p>
                    </div>
                </div>
                @foreach($projects as $project)
                <div class="col-md-6 col-lg-4">
                    <a class="animated wow fadeInUp project" href="{{ route('show_detil', ['project_id'=> $project->id] ) }}">
                        <span class="mask">
                            <span class="info">
                                <h3>{{$project->name}}</h3>
                                <p>{{$project->short_desc}}</p>
                            </span>
                           <span class="btn btn-see-project">See More</span>
                        </span>
                        <img src="{{ asset( 'storage/'. $project->main_img )}}" alt="{{$project->name}}" class="project-pic">
                    </a>
                </div>
                @endforeach
  
       
              
            </div>
        </div>

    </section>

    <!-- QUOTATION -->
    <section class="quotaion">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 ">
                        <div class="qutation-content text-center">
                            <h2>"Every exit is an entry somewhere else"</h2>
                            <p>Tom Stoppard</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- CONTACT -->
    <section id="contact-me" class="contact bounceIn wow animated">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-header text-center">
                        <h2 class="animated fadeInDown wow" data-wow-delay=".6s"><span>contact me</span></h2
                            class="animated fadeInDown wow" data-wow-delay="1s">
                        <p class="line1"></p>
                        <p class="line2"></p>
                    </div>
                </div>
                <div class="col-md-10 offset-md-1">
                    <div class="form-content">
                        <form id="contact-us-form"  method="POST" class="row">
                            <div class="form-group col-md-6">
                                <input  id="sender_name" class="form-control" type="text" name="sender_name" placeholder="Enter Name.." required>
                            </div>
                            <div class="form-group col-md-6">
                                <input id="sender_email" class="form-control" type="email" name="sender_email" placeholder="Enter Email.." required>
                            </div>
                            <div class="form-group col">
                                <textarea id="message" class="form-control textarea" name="message"
                                    placeholder="Write a message.." required></textarea>
                            </div>
                            <div class="col-12" id="send-info"></div>
                            <div class="form-group col-12">
                                <button id="send-btn" type="submit" class="btn  btn-sm btn-send">Send</button>
                            </div>
                        </form>
                        
                    </div>
                </div>
                <div>

                </div>
            </div>
        </div>
    </section>

    <!-- FOOTER -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="footer-content col-md-6">
                    <p>Phone No : {{$about->phone}}
                    <br>
                      
                    </p>
                  
                    <p>
                        Email : {{$about->email}}
                    </p>
                    </div>
                    <div class="col-md-6 text-right pt-3 copy-right">
                    <p >2020 © {{$about->name}}, All rights reserved</p>
                    </div>
                
            </div>
        </div>
</footer>


    <script src="{{ asset ('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{ asset ('js/popper.min.js')}}"></script>
    <script src="{{ asset ('js/bootstrap.min.js')}}"></script>

    <script src="{{ asset ('js/fontawesome.min.js')}}"></script>
    <script src="{{ asset ('js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset ('js/wow.min.js')}}"></script>
    <script src="{{ asset ('js/pt.min.js')}}"></script>
    <script src="{{ asset ('js/nav.js')}}"></script>
    <script src="{{ asset ('js/canvas.js')}}"></script>
    <script>
        new WOW().init();
    </script>
    <script>
        $(document).ready( function () {
   // $( "#contact-us-form" ).submit( function(e) {
      //  e.preventDefault();
      $("#loading-wrapper").hide();
        $( "#contact-us-form" ).validate({
        submitHandler: function(form) {
       $('#send-info').html('');
            var obj = {
                subject :'from my portfolio message',
                send_message :'send-message',
                sender_name:$("#sender_name").val(),
                sender_email:$("#sender_email").val(),
                message:$("#message").val(),
            };
            $("#send-btn").prop("disabled",true);
            $("#send-btn").html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> <span class="">Loading...</span>');
         //   var url ='{{URL::to('/')}}'+'/send';
            var url ='http://en.arafatthabet.dx.am/index.php/home/send_email';
            $.ajax({url:url,
                data:obj,
                type:'POST',
                dataType:'json',
                success:function (data) {
                    //data after responed , textStatus and jqXHR is object from XHHTTP class
                    console.log(data);
                    if(data.error==true){
                        $('#send-info').html('<div class="alert alert-danger alert-dismissible fade show" role="alert">'+
  ' '+data.message+
  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
    '<span aria-hidden="true">&times;</span> </button></div>');
                    }
                    else{
                        $('#send-info').html('<div class="alert alert-success alert-dismissible fade show bg-success text-white" role="alert">'+
  ' '+data.message+
  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
    '<span aria-hidden="true">&times;</span> </button></div>');
                    }
         

                    $("#send-btn").prop("disabled",false);
                    $("#send-btn").html('Send message');
                },
                statusCode: {404:function() {$('#send-info').html('<div class="alert alert-danger" style="margin-top:50px">Error Occur try again</div>');},500:function(){
                    $('#send-info').html('<div class="alert alert-danger" style="margin-top:50px">Error Occur try again </div>');
                    //alert(data);
                }}
                //alert("success");alert(data);

            });
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `invalid-feedback` class to the error element
            error.addClass( "invalid-feedback" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.next( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        }
    } );

} );

    </script>
   
</body>

</html>