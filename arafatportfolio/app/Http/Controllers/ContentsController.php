<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

use App\menu as Nav;
use App\about as About;
use App\skill as Skills;
use App\project as Project;

class ContentsController extends Controller
{
    //
    public $skill;
    public function __construct()
    {
        $this->project= new Project();
        $this->about= new About();
    }
    public function index( Request $request){

        $navbar = Nav::all();
        $about =  $this->about->get_about();
        $cv_link='';
        if($about AND $about->cv){
        $file_path ='storage/'.$about->cv;
         $file_url= asset('storage/'.$about->cv);
          if (file_exists($file_path)) {
            $cv_link=$file_url;
                       }}
        $skills = Skills::all();
        $projects = $this->project->get_projects();

        return view('home.index',
        [
            'navbar'    => $navbar,
            'about'     => $about,
            'skills'    => $skills,
            'projects'  => $projects,
            'cv_link'=>$cv_link
        ]);
    }
    
        
    // Send E-mail
    function send(Request $request)
    {
        // Make email validate
        $this->validate($request,[
        'name'          => 'required',
        'email'          => 'required|email',
        'message'          => 'required',
        ]);
        
        // bringing the reciever name and email
        $about =  $this->about->get_about();        
        $to_name = $about->name;
        $to_email = $about->email;;
        
        // Store the mail values with an array
        $data = array(
            'name'      =>  $request->name,
            'email'      =>  $request->email,
            'message'      =>  $request->message
        );

        /*Mail::send('sendemail', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('Email From My Portfolio');
            $message->from('hm@hm.com','Artisans Web');
        });*/
        
        Mail::to($to_email)->send(new SendMail($data));
        return back()->with('success','thanks for contacting me!');
    }
}
