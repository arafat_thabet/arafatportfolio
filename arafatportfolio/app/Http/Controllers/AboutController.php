<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\about as About;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class AboutController extends Controller
{
    //
    public function __construct(About $about)
    {
        $this->about = $about;
        $this->about= new About();
    }


    /*********** SHOW About FUNCTION *******/

    public function showAbout()
    {
        $about = [];
        $data = [];
        $about=$this->about->get_about();
        $About['about']= $about;
        $data['about']= $about;
        $data['edit'] = 1;
     
        $data['about_id'] = (isset($about->id)) ? $about->id : 0;
        $data['name'] = (isset($about->name)) ? $about->name : '';
        $data['phone'] = (isset($about->phone)) ? $about->phone : '' ;
        $data['email'] = (isset($about->email)) ? $about->email : '';
        $data['title'] = (isset($about->title)) ? $about->title : '';
        $data['description'] = (isset($about->description)) ? $about->description : '' ;
        $data["about_img"]=(isset($about->img)) ? asset('storage/' . $about->img ) :  asset('img/avatar.jpg');
        
        return view('about.aboutform', $data);
    }


    /********** CREATE About FUNCTION ********/

    public function addAbout(Request $request, About $about)
    {
        $data = [];
             
        $data['name'] = $request->input('name');
        $data['phone'] = $request->input('phone');
        $data['email'] = $request->input('email');
        $data['title'] = $request->input('title');
        $data['description'] = $request->input('description');

        if ($request->isMethod('post')) 
        {
            $this->validate(
                $request,
                [
                    'name' => 'required',
                    'email' => 'required',
                    'description' => 'required',
                    'img' => 'mimes:jpeg,jpg,png,bmp'
                ]
            );

            

            if ( $request->file('img')):
                        $extension = $request->file('img')->extension();
                $img_name ="myimg" . '_' . time() . rand(1, 100);
                $path = $request->file('img')->storeAs('img', $img_name.".".$extension, 'public');
                $data['img'] = $path;
            endif;

            if ( $request->fiel('cv'))
            {
                $extension = $request->file('cv')->extension();
                $file_name = 'Hamza_cv'.time().rand(1,100).".".$extension;
                $cv = $request->file('cv')->storeAs('cv', $file_name,'public');
                $data['cv'] = $cv;
            }            

            $about->insert($data);

            return Redirect('about');
        }

        $data['edit'] = 0;
        return view('about.aboutform', $data);
    }
   

    /********** EDIT ABOUT FUNCTION ********/

    public function editAbout(Request $request, $about_id)
    {
        $about_data=array();
        $about = $this->about->find($about_id);

        $data = [];
        $data['about_id'] = (isset($about->id)) ? $about->id : 0;
        $data['name'] = (isset($about->name)) ? $about->name : '';
        $data['phone'] = (isset($about->phone)) ? $about->phone : '' ;
        $data['email'] = (isset($about->email)) ? $about->email : '';
        $data['title'] = (isset($about->title)) ? $about->title : '';
        $data['description'] = (isset($about->description)) ? $about->description : '' ;
        
        if ($request->isMethod('post')) {
            $this->validate(
                $request,
                [
                    'name' => 'required',
                    'email' => 'required',
                    'description' => 'required',
                    'img' => 'mimes:jpeg,jpg,png,bmp'
                ]
            );
            $data['name'] = $request->input('name');
            $data['phone'] = $request->input('phone');
            $data['email'] = $request->input('email');
            $data['title'] = $request->input('title');
            $data['description'] = $request->input('description');
            $about_data = $this->about->find($about_id);
                if($about_data){
            $about_data->name = $request->input('name');
            $about_data->phone = $request->input('phone');
            $about_data->email = $request->input('email');
            $about_data->title = $request->input('title');
            $about_data->description = $request->input('description');
                }
                if ($request->file('img')) {
                  $extension = $request->file('img')->extension();
                    $img_name ="myimg". '_' . time() . rand(1, 100);
                    $path = $request->file('img')->storeAs('img', $img_name.".".$extension , 'public');
                   
                    if($about_data){
                        $file_path ='storage/'.$about_data->img;
                        $file_url= asset('storage/'.$about_data->img);
                       if (file_exists($file_path)) {
                        Storage::disk('public')->delete($file_path);
                            unlink($file_path);
                                  }
                        $about_data->img = $path;
                        
                        }
                        else{
                          
                            $data['img']=$path;
                        }
                }
                if ($request->file('cv'))
                {
                    $extension = $request->file('cv')->extension();
                    $file_name = 'arafat_cv'.time().rand(1,100).".".$extension;
                    $cv = $request->file('cv')->storeAs('cv', $file_name,'public');
                    if($about_data){
                        $file_path ='storage/'.$about_data->cv;
                        $file_url= asset('storage/'.$about_data->cv);
                       if (file_exists($file_path) AND $about_data->cv) {
                        Storage::disk('public')->delete($file_path);
                            unlink($file_path);
                                  }
                       
                    $about_data->cv = $cv;
                    }
                    else{
                        $data['cv']=$cv;
                    }
                } 

              
              if($about){
                $process = $about_data->save();
              }
              else{
                  unset($data['about_id']);
                  $this->about->insert($data);   
              }

            

            return redirect('about');
        }

        $data['edit'] = 1;
        return view('about.aboutform', $data);
    }


    /********** DELETE About FUNCTION ********/

    public function deleteAbout($about_id)
    {
        $deleted_about = $this->about->find($about_id)->delete();
        return Redirect('about');
    }
}
